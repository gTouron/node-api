import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  // cheackhealth method
  @Get('health')
  checkHealth(): string {
    return 'OK';
  }

  @Get('cities')
  getCities(): string[] {
    const cities = [
      'Paris',
      'Bordeaux',
      'Lyon',
      'Strasbourg',
      'Toulouse',
      'Marseille',
    ];
    return cities;
  }
}
